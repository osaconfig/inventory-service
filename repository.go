package main

import (
	invPb "gitlab.com/osaconfig/protobufs/inventory"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dbName              = "osaconfig"
	inventoryCollection = "inventory"
)

// Repository -
type Repository interface {
	GetInventory(*invPb.GetRequest) (*invPb.Inventory, error)
	CreateInventory(*invPb.Inventory) error
	RemoveInventory(*invPb.Inventory) error
	Close()
}

// InventoryRepository -
type InventoryRepository struct {
	session *mgo.Session
}

// GetInventory -
func (repo *InventoryRepository) GetInventory(inv *invPb.GetRequest) (*invPb.Inventory, error) {
	var inventory *invPb.Inventory

	err := repo.collection().Find(bson.M{
		"environment": bson.M{"$eq": inv.Environment},
	}).One(&inventory)

	if err != nil {
		return nil, err
	}

	return inventory, nil
}

// CreateInventory -
func (repo *InventoryRepository) CreateInventory(inventory *invPb.Inventory) error {
	return repo.collection().Insert(inventory)
}

// RemoveInventory -
func (repo *InventoryRepository) RemoveInventory(inventory *invPb.Inventory) error {
	return repo.collection().Remove(bson.M{"environment": bson.M{"$eq": inventory.Environment}})
}

// Close -
func (repo *InventoryRepository) Close() {
	repo.session.Close()
}

func (repo *InventoryRepository) collection() *mgo.Collection {
	return repo.session.DB(dbName).C(inventoryCollection)
}
