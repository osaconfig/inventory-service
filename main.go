package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/micro/go-micro"
	k8s "github.com/micro/kubernetes/go/micro"
	invPb "gitlab.com/osaconfig/protobufs/inventory"
)

// ConfigVars - this is the struct that contains a toplevel inventory
// that we can unmarshal the json into.
type ConfigVars struct {
	inventory invPb.Inventory
}

const (
	defaultHost = "localhost:27017"
)

func (invStruct *ConfigVars) parseInventoryJSON(m map[string]interface{}) error {
	for k, v := range m {
		switch k {
		case "_meta":
			invStruct.parseInventoryJSON(v.(map[string]interface{}))
		case "hostvars":
			hv := invPb.HostVars{}
			vmap := v.(map[string]interface{})
			for host, vars := range vmap {
				osa := invPb.OSAHost{}
				osa.OsaInventoryName = host
				varsJSON, err := json.Marshal(vars)
				if err != nil {
					log.Printf("JSON Marshal Hostvars: %v", err)
				}
				err = json.Unmarshal(varsJSON, &osa)
				if err != nil {
					log.Printf("Unmarshal Error: %v", err)
				}
				hv.OsaHosts = append(hv.OsaHosts, &osa)
			}
			invStruct.inventory.HostVars = &hv
		case "all":
			invStruct.parseInventoryJSON(v.(map[string]interface{}))
		case "vars":
			gv := invPb.GroupVars{}
			varsJSON, err := json.Marshal(v)
			if err != nil {
				log.Printf("JSON Marshal Vars: %v", err)
			}
			err = json.Unmarshal(varsJSON, &gv)
			if err != nil {
				log.Printf("Unmarshal Error: %v", err)
			}
			invStruct.inventory.GroupVars = &gv
		default:
			vmap := v.(map[string]interface{})
			if vmap["children"] != nil {
				cg := invPb.ContainerGroup{}
				cg.Name = k
				cgJSON, _ := json.Marshal(v)
				err := json.Unmarshal(cgJSON, &cg)
				if err != nil {
					log.Printf("Unmarshal Error: %v", err)
				}
				invStruct.inventory.ContainerGroups = append(invStruct.inventory.ContainerGroups, &cg)
			} else {
				hg := invPb.HostGroup{}
				hg.Name = k
				hgJSON, _ := json.Marshal(v)
				err := json.Unmarshal(hgJSON, &hg)
				if err != nil {
					log.Printf("Unmarshal Error: %v", err)
				}
				invStruct.inventory.HostGroups = append(invStruct.inventory.HostGroups, &hg)
			}
		}

	}
	return nil
}

func (invStruct *ConfigVars) loadInventory(repo Repository) {
	defer repo.Close()
	inventory := invStruct.inventory
	repo.RemoveInventory(&inventory)
	repo.CreateInventory(&inventory)
}

func main() {

	// Parse the JSON into an inventory object
	f, err := os.Open("openstack-inventory.json")
	if err != nil {
		log.Fatalf("Can't Open: %v", err)
	}
	defer f.Close()
	invJSON, _ := ioutil.ReadAll(f)
	if err != nil {
		log.Fatalf("Can't Read: %v", err)
	}
	osaInventory := ConfigVars{}
	var inventory interface{}
	err = json.Unmarshal(invJSON, &inventory)
	all := inventory.(map[string]interface{})
	osaInventory.parseInventoryJSON(all)
	osaInventory.inventory.Environment = "testing-1"

	// Database host from environment variables set in Dockerfile or spawining process
	host := os.Getenv("DB_HOST")
	// Fallback on defaultHost
	if host == "" {
		host = defaultHost
	}
	session, err := CreateSession(host)
	defer session.Close()
	if err != nil {
		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}

	srv := k8s.NewService(
		micro.Name("osaconfig.inventoryservice"),
		micro.Version("latest"),
	)

	repo := &InventoryRepository{session.Copy()}
	osaInventory.loadInventory(repo)

	srv.Init()
	invPb.RegisterInventoryServiceHandler(srv.Server(), &service{session})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
