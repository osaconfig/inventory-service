package main

import (
	"context"

	invPb "gitlab.com/osaconfig/protobufs/inventory"
	mgo "gopkg.in/mgo.v2"
)

// Our gRPC service handler
type service struct {
	session *mgo.Session
}

func (s *service) GetRepo() Repository {
	return &InventoryRepository{s.session.Clone()}
}

func (s *service) GetInventory(ctx context.Context, req *invPb.GetRequest, res *invPb.Response) error {
	defer s.GetRepo().Close()
	// Find the environment
	inv, err := s.GetRepo().GetInventory(req)
	if err != nil {
		return err
	}
	res.Inventory = inv
	return nil
}

func (s *service) CreateInventory(ctx context.Context, req *invPb.Inventory, res *invPb.Response) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().CreateInventory(req); err != nil {
		return err
	}
	res.Inventory = req
	res.Created = true
	return nil
}

func (s *service) RemoveInventory(ctx context.Context, req *invPb.Inventory, res *invPb.Response) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().RemoveInventory(req); err != nil {
		return err
	}
	res.Inventory = req
	res.Removed = true
	return nil
}
