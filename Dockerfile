FROM golang:1.12.0 as builder

WORKDIR /go/src/gitlab.com/osaconfig/inventory-service

COPY . .

RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o inventory-service .

FROM alpine:latest

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/osaconfig/inventory-service .

CMD ["./inventory-service"]
